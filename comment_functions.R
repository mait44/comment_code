## Comment maker
comment_maker <- function(df,
                          osakond.char,
                          kommentaar.char,
                          crit.char,
                          col.com.name = "Kommentaar",
                          col.group.name = "Grupp",
                          firts.col.width = 1.5,
                          seccond.col.width = 5,
                          font.size = 10){
  
  comment_data <- df[,c(osakond.char,kommentaar.char,crit.char)]
  names(comment_data)[1:2] <- c(col.group.name,col.com.name)
  
  if(is.null(crit.char) == FALSE){
    low_q <- quantile(comment_data[,3],probs = c(0.25,0.75),na.rm = TRUE)[1]
    up_q <- quantile(comment_data[,3],probs = c(0.25,0.75),na.rm = TRUE)[2]
  }
  
  comment_data <-  comment_data[comment_data[,col.com.name] != "",]
  comment_data <-  comment_data[comment_data[,col.com.name] != " ",]
  comment_data <-  comment_data[comment_data[,col.com.name] != "  ",]
  comment_data <-  comment_data[comment_data[,col.com.name] != "   ",]
  comment_data <- comment_data[order(comment_data[,col.group.name]),]
  
  
  
  
  WORD_Table = FlexTable(comment_data[,1:2])
  WORD_Table <- setFlexTableWidths(WORD_Table, widths = c(firts.col.width,seccond.col.width))
  
  
  
  
  #Header design
  WORD_Table[, 1:2, to = 'header'] = textProperties(color = "black", 
                                                    font.size = font.size, 
                                                    font.weight = "bold", 
                                                    font.family = "Calibri" )
  WORD_Table[, 2, to = 'header'] = cellProperties(vertical.align = "bottom" )
  WORD_Table[, 2, to = 'header',] = parProperties( text.align = 'left' )
  
  WORD_Table[, 1:2, to = 'header', side = "left"] <- borderProperties(style = 'none' )
  WORD_Table[, 1:2, to = 'header', side = "right"] <- borderProperties(style = 'none' )
  WORD_Table[, 1:2, to = 'header', side = 'bottom'] <- borderProperties( width=1, style = 'solid' )
  #WORD_Table[, 2, to = 'header', side = 'left'] <- borderProperties( width=1, style = 'solid' )
  
  
  #Body design
  WORD_Table[,1:2, side = "left"] <- borderProperties(style = 'none' )
  WORD_Table[,1:2, side = "right"] <- borderProperties(style = 'none' )
  WORD_Table[,1:2, side = 'bottom'] <- borderProperties(style = "solid", width=1)
  #WORD_Table[, 2,  side = 'left'] <- borderProperties( width=1, style = 'solid' )
  
  
  WORD_Table[, 2,] = parProperties( text.align = 'left' )
  WORD_Table[, 1:2,] = textProperties(color = "black", 
                                      font.size = font.size, 
                                      font.family = "Calibri" )
  WORD_Table[, 1:2,] = textProperties(color = "black", 
                                      font.size = font.size, 
                                      font.family = "Calibri" )
  
  
  
  if(is.null(crit.char) == FALSE){
    
    
    for(i in c(1:dim(comment_data)[1])){
      if(is.na(comment_data[i,3]) == TRUE){
        WORD_Table = setFlexTableBackgroundColors(WORD_Table, i = i, j = 1,colors = 'white')  
      }
      
      if(is.na(comment_data[i,3]) == FALSE){
        if(comment_data[i,3] <= low_q){
          WORD_Table = setFlexTableBackgroundColors(WORD_Table, i = i, j = 1,colors = '#a7dff9')
        }
        
        if(comment_data[i,3] >= up_q){
          WORD_Table = setFlexTableBackgroundColors(WORD_Table, i = i, j = 1,colors = '#dff9a7')
        }
        
        if(comment_data[i,3] < up_q & comment_data[i,3] > low_q){
          WORD_Table = setFlexTableBackgroundColors(WORD_Table, i = i, j = 1,colors = 'white')
        }
        
        
      }
    }
  }
  
  return(WORD_Table)
}


average_col_maker <- function(aspects.char,
                              min.answers,
                              df,
                              add.sig = TRUE,
                              sig.sufix = "_SIG"){
  #Generating working data
  if(add.sig == TRUE){
    w.data <- df[,c(paste(aspects.char,sig.sufix,sep = ""),aspects.char)]
    w.data[,"output_col_sig"] <- rep(NA, times = dim(w.data)[1])
    w.data[,"output_col_sat"] <- rep(NA, times = dim(w.data)[1])
  }
  if(add.sig == FALSE){
    w.data <- df[,aspects.char]
    w.data[,"output_col_sat"] <- rep(NA, times = dim(w.data)[1])
  }
  
  
  
  if(dim(w.data)[1] >= 1){
    for(r in c(1:dim(w.data)[1])){
      if(sum(!is.na(w.data[r,aspects.char])) >= min.answers){
        w.data[r,"output_col_sat"] <- rowMeans(w.data[r,aspects.char],na.rm = TRUE)
      }
      else if(sum(!is.na(w.data[r,aspects.char])) < min.answers){
        w.data[r,"output_col_sat"] <- NA
      }
      if(add.sig == TRUE){
        if(sum(!is.na(w.data[r,paste(aspects.char,sig.sufix,sep = "")])) >= min.answers){
          w.data[r,"output_col_sig"] <- rowMeans(w.data[r,paste(aspects.char,sig.sufix,sep = "")],na.rm = TRUE)
        }
        else if(sum(!is.na(w.data[r,paste(aspects.char,sig.sufix,sep = "")])) < min.answers){
          w.data[r,"output_col_sig"] <- NA
        }
      }
    }
  }
  
  #Returning data
  if(add.sig == TRUE){
    return(w.data[,c("output_col_sig","output_col_sat")])
  }
  if(add.sig == FALSE){
    return(w.data[,c("output_col_sat")])
  }
}